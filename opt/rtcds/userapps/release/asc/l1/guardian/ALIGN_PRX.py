# Docstring:
"""
Initial Alignment Guardian: Power Recycling X Alignment.

Author: Nathan Holland, A. Mullavey.
Date: 2019-05-13
Contact: nathan.holland@ligo.org

Modified: 2019-05-13 (Created).
Modified: 2019-05-14 (Continued writing first iteration; 
                      Planning in code comments;
                      Minor debugging).
Modified: 2019-05-14 (Continued writing first iteration; 
                      Planning in code comments;
                      Minor debugging).
Modified: 2019-05-17 (Continued writing first iteration; 
                      Planning in code comments;
                      Minor debugging).
Modified: 2019-05-20 (Continued writing first iteration; 
                      Planning in code comments;
                      Minor debugging).
Modified: 2019-05-21 (Some clean up, removing path changes).
Modified: 2019-05-22 (Changed structure to make it more self contained).
Modified: 2019-05-24 (Syntatic changes, debugging from lower level API).
Modified: 2019-05-31 (Added INIT state).
Modified: 2019-06-12 (Move towards PEP8 formatting).
Modified: 2019-07-02 (Added data collection).

                      
Comments from Corey:
 * The WFS can walk away from PRM alignment.
 * This is because no signal and no error signal both give 0.
Here are some ways to avoid this:
 > Ensure that the WFS are DC centered before hand.
   - Is this a script or manually done?
 > Check that the power in PRX is not dropping below a certain level.
   - Does the current triggered servo satisfy this?
"""


#------------------------------------------------------------------------------
# Imports:

# Uses time.sleep to enforce the EPICS refresh rate.
import time

# CDS utils, needed for averaging
import cdsutils

# Import the minimum number of needed guardian subutilities.
from guardian import GuardState, Node, GuardStateDecorator

# Import the class Optic from optic.py
# Source @
# /opt/rtcds/userapps/release/asc/l1/guardian/optic.py
from optic import Optic

# Import the class TrigServo from trigservo.py.
# Source @
# /opt/rtcds/userapps/release/asc/l1/guardian/trigservo.py
from trigservo import TrigServo

# Import the LSC matrices access.
# Source @
# /opt/rtcds/userapps/release/isc/l1/guardian/isclib/matrices.py
import isclib.matrices as matrix

# Import the function align_restore from new_align_restore.py.
# Source @
# /opt/rtcds/userapps/release/sus/common/scripts/new_align_restore.py
from new_align_restore import align_restore

# Import the function align_save from new_align_save.py.
# Source @
# /opt/rtcds/userapps/release/sus/common/scripts/align_save.py
from new_align_save import align_save

# Import the EzAvg class from
# /opt/rtcds/userapps/release/isc/l1/guardian/isclib/epics_averaging.py
from isclib.epics_average import EzAvg, ezavg

#------------------------------------------------------------------------------
# Script Variables:

# The nominal state of this guardian:
nominal = "PRX_ALIGN_IDLE"
request = nominal

# Node manager to control the IMC power.
imc_node = Node("IMC_LOCK")
alsx_node = Node("ALS_XARM")
alsy_node = Node("ALS_YARM")


#------------------------------------------------------------------------------
# Convenient access for QUADs and HSTSs.
itmx = Optic("ITMX")
etmx = Optic("ETMX")
itmy = Optic("ITMY")
etmy = Optic("ETMY")
prm = Optic("PRM")
srm = Optic("SRM")

#------------------------------------------------------------------------------
# Parameters

prx_locked_threshold = 25


#------------------------------------------------------------------------------
# Functions

def IMC_4W():
    return (imc_node.arrived and (5>ezca['IMC-IM4_TRANS_SUM_OUTPUT']>3.5) and (imc_node.state == 'LOCKED_4W'))

def PRX_locked():
    p_as_c = ezca['ASC-AS_C_SUM_OUTPUT']
    p_in = ezca['IMC-IM4_TRANS_SUM_OUTPUT']
    a_value = p_as_c/p_in
    return a_value >= prx_locked_threshold

def turn_off_wfs():
    ezca.switch('ASC-PRC1_P','INPUT','OFF')
    ezca.switch('ASC-PRC1_Y','INPUT','OFF')
    # clear histories?
    #time.sleep(0.5)
    #ezca['SUS-PRM_M1_LOCK_P_RSET'] = 2
    #ezca['SUS-PRM_M1_LOCK_Y_RSET'] = 2

def turn_off_dc_centering():
    ezca.switch(":ASC-DC1_P", "INPUT", "OFF")
    ezca.switch(":ASC-DC1_Y", "INPUT", "OFF")
    ezca.switch(":ASC-DC2_P", "INPUT", "OFF")
    ezca.switch(":ASC-DC2_Y", "INPUT", "OFF")

def turn_off_prx_feedback():
    ezca.switch('LSC-PRCL','INPUT','FM4','OFF')
    ezca.switch('SUS-PR2_M2_LOCK_L','FM1','OFF','FM6','ON')
    ezca.switch('SUS-PR2_M1_LOCK_L','INPUT','OFF')
    #ezca['LSC-PRCL_TRAMP'] = 0
    ezca['LSC-PRCL_GAIN'] = 0
    time.sleep(1)
    ezca['LSC-PRCL_RSET'] = 2	   
    ezca['SUS-PR2_M2_LOCK_L_RSET'] = 2
    ezca['SUS-PR2_M1_LOCK_L_RSET'] = 2
    time.sleep(3)

#------------------------------------------------------------------------------
# Decorators

class assert_imc_4W(GuardStateDecorator):
    def pre_exec(self):
        if not IMC_4W():
            return 'SET_POWER_TO_4W'

class assert_prx_locked(GuardStateDecorator):
    def pre_exec(self):
        if not PRX_locked():
            turn_off_wfs()
            turn_off_dc_centering()
            turn_off_prx_feedback()
            time.sleep(5)
            return 'LOCK_PRX'

#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
#                                 Guardian States
#------------------------------------------------------------------------------

# Initial state - Does nothing but is defined.
class INIT(GuardState):    
    request = False
    index = 0
    
    # Main class method - Succeeds.
    def main(self):
        # Succeed.
        return True

#------------------------------------------------------------------------------
# Idle state - Does nothing, serves as a recovery state.
class PRX_ALIGN_IDLE(GuardState):
    #goto = True
    request = True
    index = 2
    
    # Main class method - Releases the IMC guardian and sets a timer.
    def main(self):
        # Release the IMC guardian.
        #imc_node.release()

        # Set a timer to wait for 3 seconds.
        self.timer["wait"] = 3

    # Run class method - wait for timer expiration.
    def run(self):
        # Check for timer expiration.
        if self.timer["wait"]:
            # Succeed.
            return True

#------------------------------------------------------------------------------
# Reset state - Undoes the changes implemented by this guardian.
class PRX_ALIGN_RESET(GuardState):
    goto = True
    request = True
    index = 1
    
    # Main class method - Reset.
    def main(self):

        # turn off WFS, DC centering and PRX loops
        turn_off_wfs()
        turn_off_dc_centering()
        turn_off_prx_feedback()
        
        # Check the request state for THIS guardian to see if the reset
        # state has been requested.
        # Doesn't work: 2019-07-02
        #if ezca.read(":GRD-ALIGN_PRX_REQUEST_N") == self.index:
            # Reset the IMC, to 10 W, ONLY if the PRX guardian has been
            # reset.
            #imc_loose.request("LOCKED_10W")

        # Reset PRCL triggering. FIXME: Don't really need to do this here
        ezca.write(":LSC-PRCL_MASK_FM4", 1)
        ezca.write(":LSC-PRCL_TRIG_THRESH_ON", 300)
        matrix.lsc_trigger["PRCL", "POPAIR_B_RF18_I"] = 1

        # Align ETMX, ITMY, ETMY, SRM
        etmx.align('P', ezca)
        itmy.align('Y', ezca)
        etmy.align('P', ezca)
        srm.align('P', ezca)

        # TODO: Determine if, and implement, any more resetting.
                
        # Set a wait timer, to wait at least 2 seconds.
        self.timer["min_wait"] = 2
    
    # Run class method - Wait for timer, and sometimes IMC.
    def run(self):
        # Check to see if the minimum timer is running.
        if not self.timer["min_wait"]:
            # Fail if it is.
            return False

        # Check to see if the reset is requested
        #if ezca.read(":GRD-ALIGN_PRX_REQUEST_N") == self.index:
            # Check to see if the IMC is in the 10 W state.
            #if imc_loose.completed:
                # Succeed.
                #return True
        #    return True
        # TODO: Add fault handling for the managed IMC guardian. 
        #else:
            # Succeed.
        #    return True
        return True

#------------------------------------------------------------------------------
# Power Down state - Instruct the IMC to move to 4 W locked.
class SET_POWER_TO_4W(GuardState):
    request = False
    index = 7
    
    # Main class method - Instruct the IMC.
    def main(self):

        # Set the IMC guardian to managed.
        imc_node.set_managed()
        # Request 4 W state.
        imc_node.set_request("LOCKED_4W")
        # Release the IMC from being managed.
        # I don't think I need to micromanage it here. 
        #FIXME: Why manage it in the first place? Answer: I think to take over control if its already being managed
        imc_node.release()
        
        # Set a timer, to periodically check, the IMC status.
        self.timer["check"] = 2
        
        # Set a maximum timeout timer, for 2 minutes.
        self.timer["timeout"] = 120
    
    # Run class method - Check the status of the IMC.
    def run(self):

        if self.timer['timeout']:
            notify("IMC_LOCKED guardian has failed to reach \"LOCKED_4W\" "+ \
                   "within 120 seconds (timeout), please investigate.")

        if not self.timer['check']:
            return

        # Check every two seconds
        if not IMC_4W():
            self.timer['check'] = 2
            return

        return True


'''
        # TODO: IMC guardian fault checking.    
        # Inspect checking timer expiration.
        if self.timer["check"]:
            # Check to see if the IMC is in the 4 W state. FIXME: Does this require management?
            if imc_node.arrived:
                # Succeed now that the IMC is in "LOCKED_4W".
                return True
            else:
                # Reset the timer if the IMC isn't ready.
                self.timer["check"] = 2
        
        # Check for maximum timeout.
        if self.timer["timeout"]:
            # Inform the operator of timeout.
            notify("IMC_LOCKED guardian has failed to reach \"LOCKED_4W\" "+ \
                   "within 120 seconds (timeout).")
'''

#------------------------------------------------------------------------------
# IMC 4 W state - Enforce completion of the IMC "LOCKED_4W" state.
''' This state is not necessary.
class PRX_4W(GuardState):    
    request = True
    index = 10
    
    # Run class method - Check that "LOCKED_4W" is complete.
    def run(self):
        # TODO: Additional IMC guardian fault checking.        
        # Handle the IMC being in the wrong state.
        if imc_node.state != "LOCKED_4W":
            # Inform the operator.
            notify("IMC_LOCKED is not in the \"LOCKED_4W\" state.")
            log("IMC_LOCKED is not in the \"LOCKED_4W\" state.")
            
            # Jump to error state.
            return "PRX_ALIGN_IDLE"
        
        # Check to see if the IMC is incomplete.
        if not imc_node.completed:
            # Do not proceed.
            return False
        else:
            # Succeed.
            return True
'''
#------------------------------------------------------------------------------

# Misalignment state - Misalign optics so that only PRX is resonant.
class MISALIGN_FOR_PRX(GuardState):
    request = True
    index = 5
    
    # Main class method - Misalign optics.
    def main(self):

        # ALS guardians should not try to lock here
        alsx_node.set_request("QPDS_LOCKED")
        alsy_node.set_request("QPDS_LOCKED")

        # Misalign ETMX, ITMY, ETMY, SRM
        etmx.misalign('P', -40, ezca)
        itmy.misalign('Y', -60, ezca)
        etmy.misalign('P', -40, ezca)
        srm.misalign('P', -1500, ezca)
        prm.align('P',ezca)
        prm.align('Y',ezca)
        
        # Set a 10 second timer.
        self.timer["wait"] = 10
    
    # Run class method - wait for timer expiration.
    def run(self):
        # Check for timer expiration.
        if self.timer["wait"]:
            # Succeed.
            return True


# Misalignment state - Misalign optics so that only PRX is resonant.
class MISALIGN_FOR_PRY(GuardState):
    request = True
    index = 6
    
    # Main class method - Misalign optics.
    def main(self):
        # Misalign ETMX, ITMY, ETMY, SRM
        etmx.misalign('P', -40, ezca)
        itmx.misalign('Y', -60, ezca)
        etmy.misalign('P', -40, ezca)
        srm.misalign('P', -1500, ezca)
        prm.align('P',ezca)
        prm.align('Y',ezca)
        
        # Set a 10 second timer.
        self.timer["wait"] = 10
    
    # Run class method - wait for timer expiration.
    def run(self):
        # Check for timer expiration.
        if self.timer["wait"]:
            # Succeed.
            return True
#------------------------------------------------------------------------------
# Lock Acquisition state - Acquire lock for PRX.

class LOCK_PRX(GuardState):
    request = False
    index = 9
    
    @assert_imc_4W
    def main(self):
        # Remove PRCL triggering.
        matrix.lsc_trigger.zero(row = "PRCL")
        ezca.write(":LSC-PRCL_TRIG_THRESH_ON", -1e4)
        ezca.write(":LSC-PRCL_MASK_FM4", 0)
        
        # Get the PRCL filter.
        self.prcl_filt = ezca.get_LIGOFilter(":LSC-PRCL")
        
        # Switch the PRCL filter.
        self.prcl_filt.switch_off("INPUT", "FM4", engage_wait = True)
        self.prcl_filt.switch_on("FM3", "FM6", "FM8", "FM9", "OUTPUT",
                                 engage_wait = True)
        
        # Reset the PRCL filter history.
        ezca.write(":LSC-PRCL_RSET", 2)
                
        # Reset the PRM M2 L LOCK filter history. #FIXME: Not using PRM M2 anymore
        ezca.write(":SUS-PRM_M2_LOCK_L_RSET", 2)
        
        # Set the PRCL ramp time to zero.
        ezca.write(":LSC-PRCL_TRAMP", 0)
        
        # Ramp the PRCL gain, waiting for completion.
        self.prcl_filt.ramp_gain(-2000, ramp_time = 0, wait = False);
        
        # Turn the PRCL filter input on.
        self.prcl_filt.switch_on("INPUT", engage_wait = False);
        
        log("PRX locking.")

        # Flag to do items once.
        self.do_once = True
        # Set some timers
        self.timer["wait"] = 2		#Wait for changes to take effect
        
        self.prxAvg = EzAvg(ezca,1,'ASC-AS_C_SUM_OUTPUT')

    
    @assert_imc_4W
    def run(self):
        
        # Check if minimum wait time is running.
        if not self.timer["wait"]:
            # Do not proceed.
            return False

        # Check AS-C sum to see if PRX locks
        [done,power_as_c] = self.prxAvg.ezAvg()
        if not done:
            notify('Filling up EzAvg buffer')
            return
        #power_as_c = cdsutils.avg(1, 'ASC-AS_C_SUM_OUTPUT')

        p_in = ezca['IMC-IM4_TRANS_SUM_OUTPUT']
        if not (power_as_c/p_in > prx_locked_threshold):
            return False
        
        # Section for things that only need doing once in the run state.
        # FIXME: Probably unnecessary
        if self.do_once:
            # Unset the do once flag.
            self.do_once = False
            # Switch off PR2 FM6.
            ezca.switch(":SUS-PR2_M2_LOCK_L", "FM6", "OFF")
            # Switch on the PR2 integrator.
            ezca.switch(":SUS-PR2_M2_LOCK_L", "FM1", "ON")
            # Switch on the PRCL integrator.
            self.prcl_filt.switch_on("FM4", engage_wait = False)
            # Engage PR2 M1 offload (only needed if ground motion high)
            ezca.switch('SUS-PR2_M1_LOCK_L','INPUT','ON')
        
        # Succeed.
        return True
'''
class LOCK_PRY(GuardState):
    request = False
    index = 11
    
    @assert_imc_4W
    def main(self):
        # Remove PRCL triggering.
        matrix.lsc_trigger.zero(row = "PRCL")
        ezca.write(":LSC-PRCL_TRIG_THRESH_ON", -1e4)
        ezca.write(":LSC-PRCL_MASK_FM4", 0)
        
        # Get the PRCL filter.
        self.prcl_filt = ezca.get_LIGOFilter(":LSC-PRCL")
        
        # Switch the PRCL filter.
        self.prcl_filt.switch_off("INPUT", "FM4", engage_wait = True)
        self.prcl_filt.switch_on("FM3", "FM6", "FM8", "FM9", "OUTPUT",
                                 engage_wait = True)
        
        # Reset the PRCL filter history.
        ezca.write(":LSC-PRCL_RSET", 2)
                
        # Reset the PRM M2 L LOCK filter history. #FIXME: Not using PRM M2 anymore
        ezca.write(":SUS-PRM_M2_LOCK_L_RSET", 2)
        
        # Set the PRCL ramp time to zero.
        ezca.write(":LSC-PRCL_TRAMP", 0)
        
        # Ramp the PRCL gain, waiting for completion.
        self.prcl_filt.ramp_gain(-2000, ramp_time = 1, wait = False);
        
        # Turn the PRCL filter input on.
        self.prcl_filt.switch_on("INPUT", engage_wait = False);
        
        log("SRX locking.")

        # Flag to do items once.
        self.do_once = True
        # Set some timers
        self.timer["wait"] = 2		#Wait for changes to take effect
        
    
    @assert_imc_4W
    def run(self):
        
        # Check if minimum wait time is running.
        if not self.timer["wait"]:
            # Do not proceed.
            return False

        # Check AS-C sum to see if PRX locks
        power_as_c = cdsutils.avg(-1, 'ASC-AS_C_SUM_OUTPUT')
        p_in = ezca['IMC-IM4_TRANS_SUM_OUTPUT']
        if not (power_as_c/p_in > prx_locked_threshold):
            return False
        
        # Section for things that only need doing once in the run state.
        # FIXME: Probably unnecessary
        if self.do_once:
            # Unset the do once flag.
            self.do_once = False
            # Switch off PR2 FM6.
            ezca.switch(":SUS-PR2_M2_LOCK_L", "FM6", "OFF")
            # Switch on the PR2 integrator.
            ezca.switch(":SUS-PR2_M2_LOCK_L", "FM1", "ON")
            # Switch on the PRCL integrator.
            self.prcl_filt.switch_on("FM4", engage_wait = False)
            # Engage PR2 M1 offload (only needed if ground motion high)
            ezca.switch('SUS-PR2_M1_LOCK_L','INPUT','ON')
        
        # Succeed.
        return True
'''
#------------------------------------------------------------------------------

# PRX lock state - Ensure that PRX locking completes.
class PRX_LOCKED(GuardState):
    request = True
    index = 10
    
    # Main class method - Temporary for data collection.
    @assert_prx_locked
    def main(self):
        # Flag for first run iteration.
        self.first_run = True

        # Set a timer to collect data.
        self.timer["collect"] = 10
    
    # Run class method - Check to ensure that PRX lock acquisition completes.
    @assert_prx_locked
    def run(self):
        # TODO: Use a guardstate decorator to enforce PRX lock.
        
        # TODO: Check the PRX locked conditions. Then set a wait timer,
        # while setting the timer_set flag to True. Continue to monitor
        # the lock conditions until the timer expires, then succeed.
        
        # Section for things that only need doing once in the run state.
        if self.first_run:
            # Unset first run flag.
            self.first_run = False
            # Log the header.
#            header = "TRANS, ERR"
#            log(header)

#FIXME: log data in text files, not in the guardian log 
#        # Collect the data.
#        data = "DATA: {0:.3e}".format(ezca.read(":ASC-AS_C_SUM_OUTPUT")) + \
#               ", {0:.3e}".format(ezca.read(":LSC-PRCL_INMON"))
#        log(data)
#        
#        # Check for collection timer execution.
#        if not self.timer["collect"]:
#            # Don't proceed.
#            return False
                
        # For now just succeed.
        return True


#------------------------------------------------------------------------------
# AS Port check state - Ensure that the AS port is round.
#FIXME: Make this a check of initial conditions instead, i.e. light on QPDs, 
# PRM and PR2 not in weeds, PRX still locked, etc.
class CHECK_AS_PORT_ROUND(GuardState):
    request = False
    index = 12
    
    # Main class method - Setup for checking the AS port condition.
    @assert_prx_locked
    def main(self):
        # TODO: Use a guardstate decorator to enforce PRX lock. Needs
        # to throw to manual alignment here.
        
        # TODO: Somehow determine that the AS port is round.
        # Could be based upon a power measurement, of the fundamental
        # mode.
        
        # For now collect data.
        # Flag for first run iteration.
        self.first_run = True
        
        # Set a timer to collect data.
        self.timer["collect"] = 10
    
    
    # Run class method -
    @assert_prx_locked
    def run(self):
        # TODO: Use a guardstate decorator to enforce PRX lock.
        
        # TODO: Check that the AS port round condition is met.
        
        
        # Check to see if it is the first run.
        if self.first_run:
            # Unset first run flag.
            self.first_run = False
            
            
#            # Log the header.
#            header = "TRANS, ERR"
#            log(header)

#FIXME: log data in text files, not in the guardian log         
#        # Collect the data.
#        data = "DATA: {0:.3e}".format(ezca.read(":ASC-AS_C_SUM_OUTPUT")) + \
#               ", {0:.3e}".format(ezca.read(":LSC-PRCL_INMON"))
#        log(data)
#        
#        # Check for collection timer execution.
#        if not self.timer["collect"]:
#            # Don't proceed.
#            return False
        
        # For now just succeed.
        return True


#------------------------------------------------------------------------------
# Center the beams on the WFS.
class ENGAGE_DC_CENTERING(GuardState):
    request = False
    index = 17

    # Main class method - Setup for the WFS feedback.
    # TODO: First check that there is light on diodes, or do this above
    @assert_prx_locked
    def main(self):

        # Switch on the WFS centering.
        ezca.switch(":ASC-DC1_P", "INPUT", "ON")
        ezca.switch(":ASC-DC1_Y", "INPUT", "ON")
        ezca.switch(":ASC-DC2_P", "INPUT", "ON")
        ezca.switch(":ASC-DC2_Y", "INPUT", "ON")

        self.timer['wait'] = 5

    @assert_prx_locked
    def run(self):
        if not self.timer['wait']:
            return

        return True

#------------------------------------------------------------------------------
# Feedback the WFS to PRM M1. Assuming that PRM is setup correctly.
class ENGAGE_WFS_SERVO(GuardState):
    request = False
    index = 18

    @assert_prx_locked
    def main(self):

        dof = 'PRC1'

        # Set the input matrix
        matrix.asc_input_pit.zero(row=dof)
        matrix.asc_input_yaw.zero(row=dof)
        matrix.asc_input_pit[dof,'REFL_A_RF9_I'] = 1
        matrix.asc_input_yaw[dof,'REFL_A_RF9_I'] = 1

        #Set the output matrix
        matrix.asc_output_pit.zero(col=dof)
        matrix.asc_output_yaw.zero(col=dof)
        matrix.asc_output_pit['PRM',dof] = 1
        matrix.asc_output_yaw['PRM',dof] = 1

        #Turn off all dof bank filters
        ezca.switch('ASC-'+dof+'_P','FMALL','INPUT','OFF','OUTPUT','ON')
        ezca.switch('ASC-'+dof+'_Y','FMALL','INPUT','OFF','OUTPUT','ON')
        
        #Set the PRC1 gains (sets how fast the PRM is aligned)
        ezca['ASC-'+dof+'_P_GAIN'] = -0.01	#-0.03
        ezca['ASC-'+dof+'_Y_GAIN'] = 0.03	#0.1

        time.sleep(2)
        ezca.switch('ASC-'+dof+'_P','INPUT','ON')
        ezca.switch('ASC-'+dof+'_Y','INPUT','ON')

        self.chans = []
        for dof in ['PIT','YAW']:
            self.chans.append('ASC-REFL_A_RF9_I_{}_OUTPUT'.format(dof))
        self.chans.append('ASC-AS_C_SUM_OUTPUT')

        self.prxascAvgs = EzAvg(ezca,30,self.chans)

        #self.timer['wait'] = 20

    @assert_prx_locked
    def run(self):

        #avgs = cdsutils.avg(3,self.chans)

        [done,vals] = self.prxascAvgs.ezAvg()
        if not done:
            notify('Filling EzAvg buffer')
            return

        refl_wfs_pit = vals[0] #avgs[0]
        refl_wfs_yaw = vals[1] #avgs[1]
        as_c_sum = vals[2] #avgs[2]
        p_in = ezca['IMC-IM4_TRANS_SUM_OUTPUT']

        if (as_c_sum/p_in) < 35:
            notify('Transmitted power, i.e. AS_C_SUM is too low')
            return

        if abs(refl_wfs_pit)>50 or abs(refl_wfs_yaw)>50:
            #log('pit is ' + str(refl_wfs_pit) + ', yaw is ' + str(refl_wfs_yaw))
            notify('waiting for WFS signals to zero')
            return

        #if not self.timer['wait']:
        #    return

        return True

#------------------------------------------------------------------------------
# Offload control signals to the alignment sliders.
class OFFLOAD_WFS(GuardState):
    request = False
    index = 19

    @assert_prx_locked
    def main(self):

        # Time to offload
        t_offload = 5

        ezca['SUS-PRM_M1_OPTICALIGN_P_TRAMP'] = t_offload
        ezca['SUS-PRM_M1_OPTICALIGN_Y_TRAMP'] = t_offload

        calP = ezca['SUS-PRM_M1_OPTICALIGN_P_GAIN']
        calY = ezca['SUS-PRM_M1_OPTICALIGN_Y_GAIN']

        chans = ['SUS-PRM_M1_LOCK_P_OUTPUT','SUS-PRM_M1_LOCK_Y_OUTPUT']
        #avgs = cdsutils.avg(5,chans)
        avgs = ezavg(ezca,10,chans)

        offloadP = avgs[0]
        offloadY = avgs[1]

        #TODO: average the control signal?
        #offloadP = ezca['SUS-PRM_M1_LOCK_P_OUTPUT']/calP
        #offloadY = ezca['SUS-PRM_M1_LOCK_Y_OUTPUT']/calY

        ezca['SUS-PRM_M1_OPTICALIGN_P_OFFSET'] += offloadP/calP
        ezca['SUS-PRM_M1_OPTICALIGN_Y_OFFSET'] += offloadY/calY

        self.timer['offload'] = t_offload

    @assert_prx_locked
    def run(self):

        if not self.timer['offload']:
            return
        # Do some checks, are we still locked?
        return True

#------------------------------------------------------------------------------
# Manual Alignment state - Allow manual alignment of PRM.
class MANUAL_PRM_ALIGNMENT(GuardState):
    request = False
    index = 15
    
    
    # Main class method - Setup for monitoring manual alignment.
    def main(self):
        # Switch off the WFS centering.
        ezca.switch(":ASC-DC1_P", "INPUT", "OFF")
        ezca.switch(":ASC-DC1_Y", "INPUT", "OFF")
        ezca.switch(":ASC-DC2_P", "INPUT", "OFF")
        ezca.switch(":ASC-DC2_Y", "INPUT", "OFF")
        
        
        # TODO: Setup to ensure that PRX is locked.
        # TODO: Setup for AS port round check.
        # TODO: Setup for PRM alignment monitoring.
        
        # Set a 15 second timer to remind the operator that we are in
        # manual alignment.
        self.timer["remind"] = 15

    # Run class method - Monitor the manual alignment progression.
    def run(self):
        # TODO: Check that PRX is still locked. If not jump back to
        # the error state.
        
        
        # Check for reminder timer expiration.
        if self.timer["remind"]:
            # Remind the operator.
            notify("PRM is in manual alignment, please manually align.")
            log("PRM is in manual alignment, please manually align.")
            
            
            # Reset the reminding timer.
            self.timer["remind"] = 15
        
        
        # TODO: Check that AS port is round. If it isn't inform the
        # operator and continue looping.
        
        
        # TODO: Monitor PRM alignment progress. Ensure that the power
        # is sufficient for automatic alignment to proceed. If it is
        # then return to the automatic alignment state, warning the
        # operator.


#------------------------------------------------------------------------------

# Aligned state - Ensure that PRX remains sufficiently aligned.
class PRM_ALIGNED(GuardState):
    request = True
    index = 20
        
    # Main class method - Setup for monitoring.
    def main(self):
        # Switch off the WFS centering.
        #ezca.switch(":ASC-DC1_P", "INPUT", "OFF")
        #ezca.switch(":ASC-DC1_Y", "INPUT", "OFF")
        #ezca.switch(":ASC-DC2_P", "INPUT", "OFF")
        #ezca.switch(":ASC-DC2_Y", "INPUT", "OFF")
        
        # Save the position of the PRM.
        align_save("PRM", ezca)
        
        # TODO: Setup for PRM alignment monitoring.
        
        # For now collect data.
        # Flag for first run iteration.
        self.first_run = True
        
        # Set a timer to collect data.
        self.timer["collect"] = 10
    
    
    # Run class method - Monitor, and ensure that PRX is aligned.
    def run(self):
        # TODO: Monitor the alignment of PRX. If it deviates too much
        # return to the error state.
        
        
        # Check to see if it is the first run.
        if self.first_run:
            # Unset first run flag.
            self.first_run = False
            
            
            # Log the header.
#            header = "TRANS, ERR, PIT_ERR, YAW_ERR"
#            log(header)
        
#        # Collect the data.
#        data = "DATA: {0:.3e}".format(ezca.read(":ASC-AS_C_SUM_OUTPUT")) + \
#               ", {0:.3e}, ".format(ezca.read(":LSC-PRCL_INMON")) + \
#               "{0:.3e}".format(ezca.read(":ASC-REFL_A_RF9_I_PIT_OUTMON")) + \
#               ", {0:.3e}".format(ezca.read(":ASC-REFL_A_RF9_I_YAW_OUTMON"))
        
#        # Check for collection timer run.
#        if not self.timer["collect"]:
#            # Log the data.
#            log(data)
        
        
        # Succeed.
        return True





#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
#                                    Edges
#------------------------------------------------------------------------------

# Allowable transitions.
edges = [
         # Recovery.
         ("INIT", "PRX_ALIGN_IDLE"),
         # IMC power down.
         ("PRX_ALIGN_IDLE", "MISALIGN_FOR_PRX"),
         ("PRX_ALIGN_IDLE", "MISALIGN_FOR_PRY",2),
         #("PRX_ALIGN_IDLE", "SET_POWER_TO_4W"),
         #("SET_POWER_TO_4W", "PRX_4W"),
         # Main procedure.
         #("PRX_4W", "MISALIGN_FOR_PRX"),
         ("SET_POWER_TO_4W", "LOCK_PRX"),
         ("MISALIGN_FOR_PRX", "LOCK_PRX"),
         ("MISALIGN_FOR_PRY", "LOCK_PRX"),
         ("LOCK_PRX", "PRX_LOCKED"),
         ("PRX_LOCKED", "CHECK_AS_PORT_ROUND"),
         #("CHECK_AS_PORT_ROUND", "MANUAL_PRM_ALIGNMENT"),
         ("CHECK_AS_PORT_ROUND", "ENGAGE_DC_CENTERING"),
         ("ENGAGE_DC_CENTERING", "ENGAGE_WFS_SERVO"),
         ("ENGAGE_WFS_SERVO", "OFFLOAD_WFS"),
         ("OFFLOAD_WFS", "PRM_ALIGNED"),
         #("PRM_ALIGNED", "PRX_ALIGN_RESET"),
         ("PRX_ALIGN_RESET","PRX_ALIGN_IDLE")
        ]


# END.
