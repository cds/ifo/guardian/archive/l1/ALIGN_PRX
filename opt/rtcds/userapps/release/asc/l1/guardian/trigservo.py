"""
A class to emulate the following script 
/usr/lib/python2.7/site-packages/cdsutils/trigservo.py

Here I implement it ONLY for one channel. It is designed to be used in guardian 
scripts.

Author: Nathan Holland.
Date: 2019-05-17
Contact: nathan.holland@ligo.org

Modified: 2019-05-17 (Created).
Modified: 2019-05-20 (Some debugging done).
Modified: 2019-05-23 (Debugging on X2 LLO).
Modified: 2019-05-24 (Debugging on X2 LLO).
Modified: 2019-05-31 (Removed unused and problematic imports).
"""
#-------------------------------------------------------------------------------
#Imports:
import time;
import numpy as np;
#
#-------------------------------------------------------------------------------
#Script variables:
__all__ = ["TrigServo"];
    #The only thing in here.
_waittime = 0.01;
    #The wait time, as per the source code.
#-------------------------------------------------------------------------------
#The TrigServo class.
class TrigServo(object):
    """
    Help on class TrigServo in trigservo.py.
    
    A single channel EPICS trigger servo analogous to the cdsutils.trigservo 
    function. However this class only allows one control channel to be used.
    """
    
    def __init__(self,
                 ezca,
                 channel = None,
                 readback = None,
                 gain = 1.0,
                 setval = 0.0,
                 triggerChannel = None,
                 triggerOn = 0,
                 triggerOff = None,
                 triggerDelay = 0,
                 triggerGreaterThan = True):
            #Need to add epics access object here!
        """
        Help on the constructor for the class TrigServo.
        
        Constructs the TrigServo class instance.
        
        Usage:
         obj = TrigServo(ezca, channel, readback, gain, setval, triggerChannel,
                         triggerOn, triggerOff, triggerDelay, 
                         triggerGreaterThan)
         obj                - An instance of the TrigServo class.
         ezca               - EPICS channel access, an instance of ezca.Ezca.
         channel            - The control channel, EPICS.
         readback           - The error channel, EPICS.
         gain               - The gain for the feedback.
         setval             - The setpoint for the feedback.
         triggerChannel     - The channel to trigger on.
         triggerOn          - The value to trigger on.
                              If triggerGreaterThan is True then the servo will 
                              trigger only if the trigger is greater than this 
                              value.
                              If triggerGreaterThan is False then the servo will 
                              trigger only if the trigger is less than this 
                              value.
         triggerOff         - The value to turn the servo off at.
                              If triggerGreaterThan is True then the servo will 
                              turn off if the trigger falls below this value.
                              If triggerGreaterThan is False then the servo will 
                              turn off if the trigger rises above this value.
         triggerDelay       - Time period to wait while trigger channel is 
                              beyond threshold before transitioning to on or 
                              off.
         triggerGreaterThan - Controls the threshholding logic for the 
                              triggering. See above, triggerOn and triggerOff, 
                              for specific description.
        """
        #
        self._epics = ezca;
            #Epics access.
        self._channel = channel;
            #The control channel.
        self._readback = readback;
            #The error channel. 
        self._gain = gain;
            #The gain for the feedback.
        self._setval = setval;
            #The set point for the feedback control.
        self._triggerChannel = triggerChannel;
            #The channel for triggering on.
        self._triggerOn = triggerOn;
            #The value to trigger on, generally will trigger if the trigger 
            #channel rises above this value.
        self._triggerDelay = triggerDelay;
            #Record the triggering delay.
        if triggerOff is None:
            self._triggerOff = triggerOn;
                #Copied across from source.
        else:
            self._triggerOff = triggerOff;
                #The value for turning the feedback off. Generally will not 
                #feedback if the trigger channel falls below this value.
        #
        if triggerGreaterThan:
            #Allows the triggering logic to be dictated.
            self._trigLogicOn = np.greater;
            self._trigLogicOff = np.less;
                #Use the numpy functions for triggering logic.
        else:
            self._trigLogicOn = np.less;
            self._trigLogicOff = np.greater;
                #Use the numpy functions for triggering logic.
        #
        if triggerChannel is None:
            self._triggerState = None;
                #The starting trigger state.
        else:
            self._triggerState = "off";
                #The staring trigger state.
        #
        self._time_prev = 0.0;
            #The time the last feedback was provided.
        self._time_trig_start = None;
            #The starting time for the current feedback.
            #None so that 0.0 - None doesn't make any sense.
    #
    
    def triggering(self):
        """
        A function to conveniently perform the triggering, if appropriate.
        
        Usage:
         obj.triggering()
         obj - An instance of the TrigServo class.
        """
        triggerValue = self._epics.read(self._triggerChannel);
            #Read the trigger channel.
        #
        if self._triggerState == "off":
            #Currently the triggering is off.
            if not self._trigLogicOn(triggerValue, self._triggerOn):
                #The trigger condition is not met.
                return;
            #
            if self._time_trig_start is None:
                #Set the triggering start time, if it isn't already.
                self._time_trig_start = time.time();
                    #The current time, that the triggering started.
            #
            if (time.time() - self._time_trig_start) > self._triggerDelay:
                #Sufficient time has passed to enavble the triggering.
                self._triggerState = "on";
                    #Enable the servo.
            #
            return;
                #Exit.
        elif self._triggerState == "on":
            #Currently the triggering is on.
            if not self._trigLogicOff(triggerValue, self._triggerOff):
                #The trigger condition is met.
                return;
            #
            self._triggerState = "off";
                #Turn the triggering off.
            #
            self._time_trig_start = None;
                #Reset the trigger start time.
            self._time_prev = 0.0;
                #Reset the previous time.
            #
            return;
                #Exit once the resetting is done.
        else:
            #No triggering.
            return;
        #
    #
    
    def step(self):
        """
        Runs one step of the triggered servo. Note that depending on the 
        settings, and channel values, there may not be any feedback provided.
        
        Usage:
         obj.step()
         obj - An instance of the TrigServo class.
        """
        #
        if self._triggerState is None:
            #No triggering check necessary.
            pass;
        else:
            #A triggering check is required.
            self.triggering();
        #
        if (self._triggerState is None) or (self._triggerState == "on"):
            #Perform the feedback calculation.
            ctrl = float(self._epics.read(self._channel));
                #Read the control channel.
            #
            time.sleep(_waittime);
                #Sleep briefly.
            #
            if self._readback:
                err = float(self._epics.read(self._readback));
                    #Read the error channel.
            else:
                err = ctrl;
                    #As per the source the error becomes the control.
            #
            err -= self._setval;
            err *= -1;
                #Adjust the error.
            #
            dt = time.time() - self._time_trig_start - self._time_prev;
                #Reset the time step.
            self._time_prev += dt;
                #Set the new previous time.
            #
            ctrl -= self._gain * dt * err;
                #Calculate the new control value.
            #
            self._epics.write(self._channel, ctrl);
                #Write the control signal to the control channel.
        else:
            #The servo isn't triggered so do nothing.
            pass;
        #
#
#-------------------------------------------------------------------------------
#END.

